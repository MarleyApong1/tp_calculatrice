let currentInput = '0';
let lastInput = '';
let operator = '';
let resultDisplayed = false;

function updateDisplay() {
    const box = document.getElementById('box');
    box.innerText = currentInput;
}

function clear_entry() {
    currentInput = '0';
    updateDisplay();
}

function button_clear() {
    currentInput = '0';
    lastInput = '';
    operator = '';
    const lastOperationHistory = document.getElementById('last_operation_history');
    lastOperationHistory.innerText = '';
    updateDisplay();
}

function backspace_remove() {
    if (currentInput.length > 1) {
        currentInput = currentInput.slice(0, -1);
    } else {
        currentInput = '0';
    }
    updateDisplay();
}

function button_number(value) {
    if (resultDisplayed) {
        currentInput = value.toString();
        resultDisplayed = false;
    } else {
        if (currentInput === '0' && value !== '.') {
            currentInput = value.toString();
        } else {
            currentInput += value.toString();
        }
    }
    updateDisplay();
}

function calculate_percentage() {
    currentInput = (parseFloat(currentInput) / 100).toString();
    updateDisplay();
}

function division_one() {
    currentInput = (1 / parseFloat(currentInput)).toString();
    updateDisplay();
}

function power_of() {
    currentInput = (parseFloat(currentInput) ** 2).toString();
    updateDisplay();
}

function square_root() {
    currentInput = Math.sqrt(parseFloat(currentInput)).toString();
    updateDisplay();
}

function plus_minus() {
    currentInput = (parseFloat(currentInput) * -1).toString();
    updateDisplay();
}

function operate(op) {
    if (operator !== '' && !resultDisplayed) {
        calculate_result();
    }
    lastInput = currentInput;
    operator = op;
    currentInput = '0';
    resultDisplayed = false;
}

function calculate_result() {
    let result;
    switch (operator) {
        case '+':
            result = parseFloat(lastInput) + parseFloat(currentInput);
            break;
        case '-':
            result = parseFloat(lastInput) - parseFloat(currentInput);
            break;
        case '*':
            result = parseFloat(lastInput) * parseFloat(currentInput);
            break;
        case '/':
            result = parseFloat(lastInput) / parseFloat(currentInput);
            break;
        default:
            return;
    }
    const lastOperationHistory = document.getElementById('last_operation_history');
    lastOperationHistory.innerText = `${lastInput} ${operator} ${currentInput} =`;
    currentInput = result.toString();
    operator = '';
    resultDisplayed = true;
    updateDisplay();
}

document.addEventListener('DOMContentLoaded', () => {
    document.getElementById('plusOp').onclick = () => operate('+');
    document.getElementById('subOp').onclick = () => operate('-');
    document.getElementById('multiOp').onclick = () => operate('*');
    document.getElementById('divOp').onclick = () => operate('/');
    document.getElementById('equal_sign').onclick = calculate_result;
    document.getElementById('dot').onclick = () => button_number('.');
    
    updateDisplay();
});
